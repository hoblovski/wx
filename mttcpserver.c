#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>

#include "mtserver.h"

static char num2hex[16] = "0123456789abcdef";

// thread function
void *connection_handler(void *);
void* stdin_handler(void* dummy) { return 0; }

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("usage: %s port", argv[1]);
        exit(1);
    }

    int sockfd;
    mtserver_setup(atoi(argv[1]), &sockfd, 3);
    mtserver_mainloop(sockfd, connection_handler, stdin_handler);

    return 0;
}

void *connection_handler(void *_sockfd)
{
    int sockfd = *(int*)_sockfd;
    int read_size;
//    char *message;
    char client_message[2000];

//    message = "Greetings! I am your connection handler\n";
//    write(sockfd, message, strlen(message));
//    message = "Now type something and i shall repeat what you type \n";
//    write(sockfd, message, strlen(message));

    while((read_size = recv(sockfd, client_message, 2000, 0)) > 0) {
        printf(">>> recv msg len=%d, hexdump as below\n********************************\n", read_size);
        for (int i = 0; i < read_size; i++) {
            char v = client_message[i];
            putchar(num2hex[(v >> 4) & 0xf]);
            putchar(num2hex[v & 0xf]);
            if (i % 16 == 15)
                putchar('\n');
            else
                putchar(' ');
        }
        printf("\n********************************\n");

//        write(sockfd, client_message, strlen(client_message));
    }

    if (read_size == 0) printf("client disconnected\n");
    else if (read_size == -1) printf("recv failed\n");

    close(sockfd);

    free(_sockfd);
    return 0;
}
