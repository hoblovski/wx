#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <assert.h>

#include "common.h"
#include "mtserver.h"

void mtserver_setup(int port, int* sockfd, int backlog) 
{
    *sockfd = socket(AF_INET, SOCK_STREAM, 0);
    assert (*sockfd >= 0);

    sockaddr_in srvaddr;
    sockaddr_create(&srvaddr, INADDR_ANY, port);
    assert (bind(*sockfd, (sockaddr*) &srvaddr, sizeof(srvaddr)) >= 0);
    listen(*sockfd, backlog);
}

void mtserver_mainloop(int sockfd,
        void* (*connection_handler)(void*),
        void* (*stdin_handler)(void*))
{
    int clifd;
    int cliaddr_sz = sizeof(sockaddr_in);
    sockaddr_in cliaddr;

    pthread_t stdin_thread;

    assert (pthread_create(&stdin_thread, NULL,
                stdin_handler, NULL) >= 0);

    while ((clifd = accept(sockfd, (sockaddr*) &cliaddr,
                    (socklen_t*) &cliaddr_sz))) {
        printf("connection accepted from %s:%d\n",
                inet_ntoa(cliaddr.sin_addr), cliaddr.sin_port);

        pthread_t sniffer_thread;
        int* _clifd = malloc(4);
        *_clifd = clifd;

        assert (pthread_create(&sniffer_thread, NULL,
                    connection_handler, (void*) _clifd) >= 0);
    }

    assert (clifd >= 0);
}




