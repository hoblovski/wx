#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "common.h"
#include "client.h"

char buffer[256];

int main(int argc, char *argv[]) 
{
    if (argc < 3) {
        fprintf(stderr, "usage %s ip port\n", argv[0]);
        exit(0);
    }
    int sockfd;
    client_setup(argv[1], atoi(argv[2]), &sockfd);

    while (1) {
        /* write to socket */
        printf("send:\n");
        bzero(buffer, sizeof(buffer));
        fgets(buffer, sizeof(buffer) - 1, stdin);
        if (!strcmp(buffer, "quit\n")) {
            printf("close: %d\n", close(sockfd));
            break;
        }
        assert (write(sockfd, buffer, strlen(buffer)) >= 0);
        printf("sent!");

//        /* read from socket */
//        printf("recv...\n");
//        bzero(buffer, 256);
//        assert (read(sockfd, buffer, 255) >= 0);
//        printf("-------\n%s\n--------", buffer);
    }

    return 0;
}
