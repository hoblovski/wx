#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define bzero(a, l) memset(a, 0, l)

typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;

/*
 * iph: in host order
 */
void sockaddr_create(sockaddr_in* addr, int iph, short port);
void sockaddr_create_ips(sockaddr_in* addr, char* ip, short port);

void putb_hex(char v);
void dump_mem(char* v, int size);

#endif // COMMON_H
