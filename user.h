#ifndef USER_H
#define USER_H

#include <stdio.h>

// use text file to manage user records
#define USER_FILE "users"
/*
 * now format of USER_FILE:
 *      four line for one record
 *      id[numeric]
 *      name[str]
 *      pswd[str]
 *      friend_usrid friend_usrid ... -1
 *
 */

#define MAX_NAME_LEN 20
#define MAX_PSWD_LEN 40


struct friend_list_t {
    int usrid;
    struct friend_list_t* next;
};
typedef struct friend_list_t friend_list_t;


struct user_t {
    int id;
    char name[MAX_NAME_LEN];
    char pswd[MAX_PSWD_LEN];
    friend_list_t* friends;
    int sockfd;
    struct user_t* next;
};
typedef struct user_t user_t;


// only one user manager
/* CRITICAL */
void init_usrmgr();

/* malloc and initialize with default values */
user_t* create_user();

/* CRITICAL */
user_t* add_user(char* name, char* pswd);

/* friend is a BIDIRECTIONAL relation. 
 * ignores if one == other
 */
/* CRITICAL */
void add_friend(user_t* one, user_t* other);

/* returns NULL if not found */
user_t* get_user(char* name, char* pswd);
user_t* get_user_byid(int id);
user_t* get_user_byname(char* name); 

/* CRITICAL */
/* write to file, and free */
void finalize_usrmgr();

/* don't use this unless you know what you are doing */
void _dump_usrmgr();

int is_logged_in(user_t* usr);

/* CRITICAL */
void login(user_t* usr);
/* CRITICAL */
void logout(user_t* usr);

#endif // USER_H
