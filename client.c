#include "common.h"
#include <assert.h>

void client_setup(char* srvip, int srvport, int* sockfd)
{
    /* create fd */
    *sockfd = socket(AF_INET, SOCK_STREAM, 0);
    assert (*sockfd >= 0);

    /* create addr */
    sockaddr_in serv_addr;
    sockaddr_create_ips(&serv_addr, srvip, srvport);

    /* connect */
    assert (connect(*sockfd, (sockaddr*) &serv_addr,
                sizeof(serv_addr)) >= 0);
}


