#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <time.h>

#include "common.h"
#include "client.h"
#include "msgbuf.h"
#include "user.h"

#define MAX_BUF_LEN_CLIENT 1024

// the unique socket used to communicate with srv
int sockfd;

int recv_file_magic;
FILE* recv_file_handle;
char recv_file_name[256];

// my id
int local_id;
char local_name[MAX_NAME_LEN];
int chat_id;
char chat_name[MAX_NAME_LEN];

char buffer[MAX_BUF_LEN_CLIENT];

// stores local friends
struct local_friend_list_t {
    int usrid;
    char* name;
    struct local_friend_list_t* next;
};
typedef struct local_friend_list_t local_friend_list_t;
local_friend_list_t* local_friends;
local_friend_list_t* pend_fr_passive;

void new_local_friends_entry(int usrid, char* name);
local_friend_list_t* get_local_friend_by_name(char* name);
local_friend_list_t* get_local_friend_by_id(int id);
void free_local_friends();
void display_friend_list();

// reset state to not logged in
void reset_state();
void client_init();
char* get_dlsvpath_fmt();

char* user_info();

void do_login();
void do_chat();
void do_ls();
void do_sendmsg();
void do_forcerecv();
void do_add();
void do_acfr();
void do_rejfr();
void do_sendfile();

void handle_recv();

int main(int argc, char* argv[])
{
    if (argc != 2 && argc != 3) {
        fprintf(stderr, "usage %s [srvip] port\n", argv[0]);
        exit(0);
    }
    client_init();
    printf("... wxclient\n");
    if (argc == 3)
        client_setup(argv[1], atoi(argv[2]), &sockfd);
    else
        client_setup("127.0.0.1", atoi(argv[1]), &sockfd);
    printf("... setup done\n");

    reset_state();

    while (1) {
        printf("%s >>> ", user_info());
        fflush(stdout);

        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(0, &readfds);
        FD_SET(sockfd, &readfds);
        select(sockfd + 1, &readfds, NULL, NULL, NULL);

        if (FD_ISSET(sockfd, &readfds)) {
            printf("\r");
            fflush(stdout); // flush original prompt
            handle_recv();
        }
        if (FD_ISSET(0, &readfds)) {
            char cmd[20];
            scanf("%s", cmd);
            if (!strcmp(cmd, "login")) {
                do_login();
            } else if (!strcmp(cmd, "quit")) {
                close(sockfd);
                break;
            } else if (!strcmp(cmd, "chat")) {
                do_chat();
            } else if (!strcmp(cmd, "forcerecv")) {
                do_forcerecv();
            } else if (!strcmp(cmd, "sendmsg")) {
                do_sendmsg();
            } else if (!strcmp(cmd, "ls")) {
                do_ls();
            } else if (!strcmp(cmd, "add")) {
                do_add();
            } else if (!strcmp(cmd, "acfr")) {
                do_acfr();
            } else if (!strcmp(cmd, "rejfr")) {
                do_rejfr();
            } else if (!strcmp(cmd, "sendfile")) {
                do_sendfile();
            } else {
                printf("unsupported command!\n");
                // TODO: display help
            }
        }
    }
}


char* user_info()
{
    // format is like   [id] (usr)
    static char info[MAX_NAME_LEN + 20];
    if (local_id == -1) {
        sprintf(info, "[-1] (guest)");
    } else {
        sprintf(info, "[%d] (%s)", local_id, local_name);
    }
    return info;
}


void new_local_friends_entry(int usrid,
        char* name)
{
    assert (usrid != -1);
    assert (name[0] != 0);
    local_friend_list_t* rv = (local_friend_list_t*)
            malloc(sizeof(local_friend_list_t));
    rv->usrid = usrid;
    rv->name = strdup(name);
    rv->next = local_friends;
    local_friends = rv;
}


void free_local_friends()
{
    local_friend_list_t *it = local_friends;
    while (it) {
        local_friend_list_t* cur = it;
        it = it->next;
        free((void*) cur);
    }
    local_friends = NULL;
}


void reset_state()
{
    free_local_friends();
    local_id = -1;
    local_friends = NULL;
    local_name[0] = 0;
    chat_id = -1;
    recv_file_magic = -1;
    recv_file_handle = NULL;
}


void do_login()
{
    // reset state to not logged in
    reset_state();

    // get name and pswd
    char name[MAX_NAME_LEN];
    char pswd[MAX_PSWD_LEN];
    scanf("%s %s", name, pswd);
    int namelen = strlen(name);
    int pswdlen = strlen(pswd);

    // make LOGIN_REQ message
    msg_t* msg = (msg_t*) buffer;
    msg->type = MSG_TYPE_LOGIN_REQ;
    msg->from_id = local_id;
    msg->to_id = -1;
    msg->data_sz = namelen + 1 + pswdlen + 1;
    char* data = msgdata(msg);
    strcpy(data, name);
    strcpy(data + namelen + 1, pswd);

    // write
    assert (msgsize(msg) < sizeof(buffer));
    int n_write = write(sockfd, (void*) msg, msgsize(msg));
    assert (n_write >= 0); // no error
    assert (n_write == msgsize(msg));

    strcpy(local_name, name);
}


void do_chat()
{
    char ch;
    while (isspace(ch = getchar()));
    chat_name[0] = ch; // ignore spaces
    fgets(chat_name + 1, sizeof(chat_name) - 1, stdin);
    if (local_id == -1) {
        printf("... you are not logged in!\n");
        fflush(stdout);
        return;
    }
    chat_name[strlen(chat_name) - 1] = '\0'; // strip '\n'
    local_friend_list_t* f = get_local_friend_by_name(chat_name);
    if (f == NULL) {
        printf("... no such friend!\n");
        fflush(stdout);
        return;
    }
    chat_id = f->usrid;
}


void handle_recv()
{

    // get response
    bzero(buffer, sizeof(buffer));
    int recv_sz = read(sockfd, buffer, sizeof(buffer));
    assert (recv_sz >= 0);  // check error
    assert (recv_sz > 0);   // check EOF
    int procd_sz = 0;
    msg_t* msg;
    char* data;


    while (procd_sz < recv_sz) {
        msg = (msg_t*) (((char*) buffer) + procd_sz);
        data = msgdata(msg);

        // handle response
        switch (msg->type) {
            case MSG_TYPE_LOGIN_RES:{
                assert (msg->from_id == -1);
                assert (msg->data_sz > 0);

                if (data[0] == DISALLOW) {
                    assert (msg->data_sz == 1);
                    printf("... login failed. check username and password. or someone has already logged in your account.\n");
                    fflush(stdout);
                    return;
                } else if (data[0] == ALLOW) {
                    local_id = msg->to_id;
                    printf("... login successful. current user: [%d] %s\n",
                            local_id, local_name);
                    fflush(stdout);
                    char* flist_entries = data + 1;
                    while (flist_entries < data + msg->data_sz) {
                        int usrid = *(int*) flist_entries;
                        char* fname = flist_entries + sizeof(int);
                        new_local_friends_entry(usrid, fname);
                        flist_entries +=
                                sizeof(int) + strlen(fname) + 1;
                    }
                } else {
                    assert(0);
                }

                break;}

            case MSG_TYPE_TEXT:{
                assert (msg->to_id == local_id);
                local_friend_list_t* f = get_local_friend_by_id(msg->from_id);
                printf("*** [%d] (%s) : ",
                        msg->from_id, f->name);
                data[msg->data_sz] = 0;
                puts(data);
                break;}

            case MSG_TYPE_FRIENDS_ACK:{
                assert (msg->to_id == local_id);
                assert (msg->from_id == -1);
                char* flist_entries = data;
                free_local_friends();
                while (flist_entries < data + msg->data_sz) {
                    int usrid = *(int*) flist_entries;
                    char* fname = flist_entries + sizeof(int);
                    new_local_friends_entry(usrid, fname);
                    flist_entries += sizeof(int) + strlen(fname) + 1;
                }
                display_friend_list();
                fflush(stdout);
                break;}

            case MSG_TYPE_FRIEND_REQ:{
                assert (msg->to_id == local_id);
                local_friend_list_t* f;
                f = malloc(sizeof(local_friend_list_t));
                f->usrid = msg->from_id;
                f->name = strdup(msgdata(msg));
                f->next = pend_fr_passive;
                pend_fr_passive = f;
                printf("*** friend request from [%d] (%s).\n*** use `acfr %s` to accept him, or `rejfr %s` to reject.\n",
                        f->usrid, f->name, f->name, f->name);
                break;}

            case MSG_TYPE_FRIEND_ACK:{
                assert (msg->to_id == local_id);
                switch (data[0]) {
                    case FRIEND_ACK_REJECT:{
                        printf("*** (%d) [%s] rejected your invitation\n",
                                msg->from_id, (data + 1));
                        break;}
                    case FRIEND_ACK_ALLOW:{
                        printf("*** (%d) [%s] accepted your invitation\n",
                                msg->from_id, (data + 1));
                        new_local_friends_entry(msg->from_id, data + 1);
                        break;}
                    case FRIEND_ACK_NO_SUCH_USR:{
                        printf("*** no such user!\n");
                        break;}
                    default:{
                        assert (0);}
                }
                break;}

            case MSG_TYPE_FILE:{
                assert (msg->to_id == local_id);
                int* magic = (int*) data;
                int* file_status_code = (int*) (data + sizeof(int));
                switch (*file_status_code) {
                    case FILE_BEGIN:{
                        char* file_name = data + sizeof(int) + sizeof(int);
                        strcpy(recv_file_name, file_name);
                        local_friend_list_t* from_usr = get_local_friend_by_id(msg->from_id);
                        if (from_usr == NULL) break;
                        printf("*** [%d] (%s) : file '%s'\n",
                                from_usr->usrid, from_usr->name, file_name);
                        recv_file_magic = *magic;
                        char path[256];
                        sprintf(path, get_dlsvpath_fmt(), file_name);
                        recv_file_handle = fopen(path, "wb");
                        break;}
                    case FILE_TRANSFER:{
                        if (recv_file_magic != *magic) break;
                        int remain = msg->data_sz - sizeof(int) - sizeof(int);
                        char* cont = data + sizeof(int) + sizeof(int);
                        while (remain != 0) {
                            remain -= fwrite(cont, 1, remain, recv_file_handle);
                        }
                        break;}
                    case FILE_END:{
                        if (recv_file_magic != *magic) break;
                        recv_file_magic = -1;
                        printf("*** completed receiving '%s' from [%d]\n",
                                recv_file_name, msg->from_id);
                        fclose(recv_file_handle);
                        break;}
                    default:{
                        assert(0);
                        break;}
                }
                break;}

            default:{
                printf("[recv] | unhandled msg, dump\n");
                printf("[recv] | type: %d\n", msg->type);
                printf("[recv] | from_id: %d\n", msg->from_id);
                printf("[recv] | to_id: %d\n", msg->to_id);
                printf("[recv] | data_sz: %d\n", msg->data_sz);
                printf("[recv] | data dump ********\n");
                dump_mem(msgdata(msg), msg->data_sz);
                printf("[recv] | ********\n");
                break;}
        }

        procd_sz += msgsize(msg);
    }
}


local_friend_list_t* get_local_friend_by_name(char* name)
{
    local_friend_list_t* f = local_friends;
    while (f) {
        if (!strcmp(name, f->name))
            return f;
        f = f->next;
    }
    return NULL;
}


void display_friend_list() {
    printf("... my friends:\n");
    local_friend_list_t* f = local_friends;
    while (f) {
        printf("...  - [id=%d] (name=%s) \n", f->usrid, f->name);
        f = f->next;
    }
}


void do_ls()
{
    msg_t* msg = (msg_t*) buffer;
    msg->type = MSG_TYPE_FRIENDS_REQ;
    msg->from_id = local_id;
    msg->to_id = -1;
    msg->data_sz = 0;

    if (local_id == -1) {
        printf("... you are not logged in!\n");
        return;
    }

    int n_write = write(sockfd, (void*) msg, msgsize(msg));
    assert (n_write >= 0);
    assert (n_write == msgsize(msg));
}


void do_sendmsg()
{
    msg_t* msg = (msg_t*) buffer;
    char* data = msgdata(msg);
    msg->type = MSG_TYPE_TEXT;
    msg->from_id = local_id;
    msg->to_id = chat_id;
    char ch;
    while (isspace(ch = getchar()));
    data[0] = ch; // ignore spaces
    fgets(data + 1, sizeof(buffer) - 1 - get_msgsize(0), stdin);
    msg->data_sz = strlen(data) - 1;
    data[msg->data_sz] = 0; // strip '\n'

    if (local_id == -1) {
        printf("... you are not logged in!\n");
        return;
    }
    if (chat_id == -1) {
        printf("... use `chat NAME` to tell me to whom you are chatting\n");
        return;
    }

    assert (msgsize(msg) < sizeof(buffer));
    int n_write = write(sockfd, (void*) msg, msgsize(msg));
    assert (n_write >= 0); // no error
    assert (n_write == msgsize(msg));
}

void do_forcerecv() {
    bzero(buffer, sizeof(buffer));
    int recv_sz;
    printf("... force recv, waiting...\n");
    recv_sz = read(sockfd, buffer, sizeof(buffer) - 1);
    printf("... force recv, got %d bytes!\n", recv_sz);
}


local_friend_list_t* get_local_friend_by_id(int id)
{
    local_friend_list_t* f = local_friends;
    while (f) {
        if (f->usrid == id)
            return f;
        f = f->next;
    }
    return NULL;
}


void do_add()
{
    char name[MAX_NAME_LEN];
    char ch;
    while (isspace(ch = getchar()));
    name[0] = ch; // ignore spaces
    fgets(name + 1, sizeof(name) - 1, stdin);
    int name_len = strlen(name);
    name[name_len - 1] = '\0'; // strip '\n'

    // make FRIEND_REQ message
    msg_t* msg = (msg_t*) buffer;
    char* data = msgdata(msg);
    msg->type = MSG_TYPE_FRIEND_REQ;
    msg->from_id = local_id;
    msg->to_id = -1; // don't know id, only name given
    msg->data_sz = name_len;
    strcpy(data, name);

    if (local_id == -1) {
        printf("... you are not logged in.\n");
        return;
    }
    local_friend_list_t* tgtusr = get_local_friend_by_name(name);
    if (tgtusr != NULL) {
        printf("... you are already friends!\n");
        return;
    }

    int n_write = write(sockfd, (void*) msg, msgsize(msg));
    assert (n_write >= 0);
    assert (n_write == msgsize(msg));
}


void do_acfr()
{
    char name[MAX_NAME_LEN];
    char ch;
    while (isspace(ch = getchar()));
    name[0] = ch; // ignore spaces
    fgets(name + 1, sizeof(name) - 1, stdin);
    int name_len = strlen(name);
    name[name_len - 1] = '\0'; // strip '\n'

    local_friend_list_t* f = pend_fr_passive;
    local_friend_list_t* prev = NULL;
    while (f != NULL) {
        if (!strcmp(name, f->name)) {
            // make FRIEND_ACK-allow msg
            msg_t* msg = (msg_t*) buffer;
            char* data = msgdata(msg);
            msg->type = MSG_TYPE_FRIEND_ACK;
            msg->from_id = local_id;
            msg->to_id = f->usrid; // don't know id, only name given
            msg->data_sz = 1;
            data[0] = FRIEND_ACK_ALLOW;
            // send msg
            int n_write = write(sockfd, (void*) msg, msgsize(msg));
            assert (n_write >= 0);
            assert (n_write == msgsize(msg));
            // update local friend table
            new_local_friends_entry(f->usrid, f->name);
            if (prev == NULL) {
                pend_fr_passive = f->next;
            } else {
                prev->next = f->next;
            }
            free(f);
            return;
        }
        prev = f;
        f = f->next;
    }
    printf("... %s is not waiting to be your friend ;)\n", name);
}


void do_rejfr()
{
    char name[MAX_NAME_LEN];
    char ch;
    while (isspace(ch = getchar()));
    name[0] = ch; // ignore spaces
    fgets(name + 1, sizeof(name) - 1, stdin);
    int name_len = strlen(name);
    name[name_len - 1] = '\0'; // strip '\n'

    local_friend_list_t* f = pend_fr_passive;
    while (f != NULL) {
        if (!strcmp(name, f->name)) {
            // make FRIEND_ACK-allow msg
            msg_t* msg = (msg_t*) buffer;
            char* data = msgdata(msg);
            msg->type = MSG_TYPE_FRIEND_ACK;
            msg->from_id = local_id;
            msg->to_id = f->usrid; // don't know id, only name given
            msg->data_sz = 1;
            data[0] = FRIEND_ACK_REJECT;
            // send msg
            int n_write = write(sockfd, (void*) msg, msgsize(msg));
            assert (n_write >= 0);
            assert (n_write == msgsize(msg));
            return;
        }
        f = f->next;
    }
    printf("... %s is not waiting to be your friend ;)\n", name);
}


void do_sendfile()
{
    char path[256];
    char ch;
    while (isspace(ch = getchar()));
    path[0] = ch; // ignore spaces
    fgets(path + 1, sizeof(path) - 1, stdin);
    int path_len = strlen(path);
    path[path_len - 1] = '\0'; // strip '\n'

    msg_t* msg = (msg_t*) buffer;
    char* data = msgdata(msg);

    if (local_id == -1) {
        printf("... you are not logged in.\n");
        return;
    }
    if (chat_id == -1) {
        printf("... use `chat NAME` to tell me to whom you are chatting\n");
        return;
    }

    FILE* fin = fopen(path, "rb");
    int magic = (rand() << 16) ^ rand();
    int n_write;

    msg->type = MSG_TYPE_FILE;
    msg->from_id = local_id;
    msg->to_id = chat_id;
    // initial msg: magic + BEGIN + filename
    *(int*) data = magic;
    *(int*) (data + sizeof(int)) = FILE_BEGIN;
    char* it = path;
    char* fname = path;
    while (*it) {
        if (*it == '/')
            fname = it + 1;
        it++;
    }
    strcpy(data + sizeof(int) + sizeof(int), fname);
    msg->data_sz = sizeof(int) + sizeof(int) + strlen(fname) + 1;

    n_write = write(sockfd, (void*) msg, msgsize(msg));
    assert (n_write >= 0);
    assert (n_write == msgsize(msg));

    char ack_buf[10];
    read(sockfd, ack_buf, sizeof(ack_buf) - 1);
    // wait for server ack to send next packet
    while (1) {
        msg = (msg_t*) buffer;
        msg->type = MSG_TYPE_FILE;
        msg->from_id = local_id;
        msg->to_id = chat_id;
        data = msgdata(msg);
        *(int*) data = magic;
        *(int*) (data + sizeof(int)) = FILE_TRANSFER;

        msg->data_sz = sizeof(int) + sizeof(int);
        char* cont = data + msg->data_sz;
        // remaining bytes to fill with the file's bytes
        int slack = buffer + sizeof(buffer) - cont;
        int n_freadtot = 0;
        while (slack >= FILE_SEND_SLACK) {
            size_t n_fread = fread(cont,
                    1,
                    slack,
                    fin);
            cont += n_fread;
            slack -= n_fread;
            n_freadtot += n_fread;
            if (n_fread == 0) break;
        }
        msg->data_sz += n_freadtot;

        assert (n_freadtot >= 0);
        if (n_freadtot == 0) break;

        n_write = write(sockfd, (void*) msg, msgsize(msg));
        assert (n_write >= 0);
        assert (n_write == msgsize(msg));

        read(sockfd, ack_buf, sizeof(ack_buf) - 1);
        // wait for server ack to send next packet
    }

    // end msg: magic + END
    *(int*) data = magic;
    *(int*) (data + sizeof(int)) = FILE_END;
    msg->data_sz = sizeof(int) + sizeof(int);

    n_write = write(sockfd, (void*) msg, msgsize(msg));
    assert (n_write >= 0);
    assert (n_write == msgsize(msg));

    fclose(fin);
}


void client_init()
{
    srand(time(0));
}


char* get_dlsvpath_fmt()
{
    static char fmt[256];
    strcpy(fmt, getenv("HOME"));
    strcpy(fmt + strlen(fmt), "/Downloads/%s");
    return fmt;
}
