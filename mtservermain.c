#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/syscall.h>
#include <signal.h>
#include <assert.h>

#include "common.h"
#include "mtserver.h"
#include "msgbuf.h"
#include "user.h"

#define MAX_BUF_LEN_SERVER 1024

// thread function
void *connection_handler(void*);
void *stdin_handler(void*);

int main(int argc, char *argv[])
{
    init_usrmgr();
    init_msgbuf();

    if (argc != 2) {
        printf("usage: %s port", argv[1]);
        exit(1);
    }

    int sockfd;
    mtserver_setup(atoi(argv[1]), &sockfd, 3);
    mtserver_mainloop(sockfd, connection_handler, stdin_handler);

    return 0;
}

void *connection_handler(void *_sockfd)
{
    pid_t tid = syscall(SYS_gettid);
    int sockfd = *(int*)_sockfd;
    int read_size;
    // use a buffer for each socket, as stack variable
    char buffer[MAX_BUF_LEN_SERVER];

    user_t* usr = NULL;

    while ((read_size = recv(sockfd, buffer, sizeof(buffer), 0)) > 0) {
        msg_t* msg = (msg_t*) buffer;
        char* data = msgdata(msg);
        switch (msg->type) {
            case MSG_TYPE_LOGIN_REQ:{
                printf("[%d] login request ", tid);
                if (usr != NULL) {
                    usr->sockfd = -1;
                    usr = NULL;
                }
                //
                // parse message
                assert (msg->from_id == -1);
                assert (msg->to_id == -1);
                char name[MAX_NAME_LEN];
                char pswd[MAX_PSWD_LEN];
                int namelen = -1;
                char* data = msgdata(msg);
                strcpy(name, data);
                namelen = strlen(name);
                strcpy(pswd, data + namelen + 1);
                //
                // check if authenticated, construct response
                user_t* new_usr = get_user(name, pswd);
                if (new_usr == NULL) {
                    // not authenticated
                    printf("failed\n");
                    msg->type = MSG_TYPE_LOGIN_RES;
                    msg->to_id = -1;
                    msg->from_id = -1;
                    msg->data_sz = 1;
                    data[0] = DISALLOW;
                } else if (new_usr->sockfd != -1) {
                    printf("failed\n");
                    msg->type = MSG_TYPE_LOGIN_RES;
                    msg->to_id = -1;
                    msg->from_id = -1;
                    msg->data_sz = 1;
                    data[0] = DISALLOW;
                    write(sockfd, (void*) msg, msgsize(msg));
                } else {
                    printf("succeeded\n");
                    // authenticated
                    assert (!strcmp(new_usr->name, name));
                    assert (!strcmp(new_usr->pswd, pswd));
                    new_usr->sockfd = sockfd;
                    msg->type = MSG_TYPE_LOGIN_RES;
                    msg->to_id = new_usr->id;
                    msg->from_id = -1;
                    msg->data_sz = 1; // ALLOW
                    // construct data
                    data[0] = ALLOW;
                    char* itptr_friends = data + 1; // friend list
                    friend_list_t* f = new_usr->friends;
                    while (f != NULL) {
                        *(int*) itptr_friends = f->usrid;
                        user_t* fusr = get_user_byid(f->usrid);
                        int fnamelen = strlen(fusr->name);
                        strcpy(itptr_friends + sizeof(int), fusr->name);
                        itptr_friends += sizeof(int) + fnamelen + 1;
                        msg->data_sz += sizeof(int) + fnamelen + 1;
                        f = f->next;
                    }
                    write(sockfd, (void*) msg, msgsize(msg));
                    // now send all pending msg
                    msg_list_t* pending_msgs = NULL;
                    pull(new_usr->id, &pending_msgs);
                    msg_list_t* it = pending_msgs;
                    while (it) {
                        msg = it->msg;
                        write(sockfd, (void*) msg, msgsize(msg));
                        it = it->next;
                    }
                    free_msglist(pending_msgs, 0);
                    if (usr != NULL)
                        usr->sockfd = -1;
                    usr = new_usr;
                }
                break;}

            case MSG_TYPE_LOGIN_RES:{
                printf("[%d] invalid msgtype: MSG_TYPE_LOGIN_RES\n", tid);
                break;}

            case MSG_TYPE_TEXT:{
                assert (msg->from_id == usr->id);
                assert (msg->to_id != -1);
                user_t* dstusr = get_user_byid(msg->to_id);
                assert (dstusr != NULL);
                if (dstusr->sockfd != -1) {
                    // dest online
                    write(dstusr->sockfd, (void*) msg, msgsize(msg));
                } else {
                    add_msg(msg);
                    // dest offline
                }
                break;}

            case MSG_TYPE_FRIENDS_REQ:{
                assert (msg->from_id == usr->id);
                assert (msg->to_id == -1);
                assert (msg->data_sz == 0);

                msg->type = MSG_TYPE_FRIENDS_ACK;
                msg->from_id = -1;
                msg->to_id = usr->id;
                msg->data_sz = 0;
                char* itptr_friends = data; // friend list
                friend_list_t* f = usr->friends;
                while (f != NULL) {
                    *(int*) itptr_friends = f->usrid;
                    user_t* fusr = get_user_byid(f->usrid);
                    int fnamelen = strlen(fusr->name);
                    strcpy(itptr_friends + sizeof(int), fusr->name);
                    itptr_friends += sizeof(int) + fnamelen + 1;
                    msg->data_sz += sizeof(int) + fnamelen + 1;
                    f = f->next;
                }
                write(sockfd, (void*) msg, msgsize(msg));
                break;}

            case MSG_TYPE_FRIENDS_ACK:{
                printf("[%d] invalid msgtype: MSG_TYPE_FRIENDS_ACK\n", tid);
                break;}

            case MSG_TYPE_FRIEND_REQ:{
                assert (msg->from_id == usr->id);
                assert (msg->to_id == -1);
                assert (data[msg->data_sz - 1] == 0);
                user_t* tgtusr = get_user_byname(data);
                if (tgtusr == NULL) {
                    // no such user
                    msg->type = MSG_TYPE_FRIEND_ACK;
                    msg->to_id = usr->id;
                    msg->from_id = -1;
                    msg->data_sz = 1;
                    data[0] = FRIEND_ACK_NO_SUCH_USR;
                    write(sockfd, (void*) msg, msgsize(msg));
                } else {
                    msg->type = MSG_TYPE_FRIEND_REQ;
                    msg->to_id = tgtusr->id;
                    msg->from_id = usr->id;
                    msg->data_sz = strlen(usr->name) + 1;
                    strcpy(data, usr->name);
                    if (tgtusr->sockfd != -1) {
                        write(tgtusr->sockfd, (void*) msg, msgsize(msg));
                    } else {
                        add_msg(msg);
                    }
                }
                break;}

            case MSG_TYPE_FRIEND_ACK:{
                assert (msg->from_id == usr->id);
                assert (msg->data_sz == 1);
                user_t* tgtusr = get_user_byid(msg->to_id);
                assert (tgtusr != NULL);
                switch (data[0]) {
                    case FRIEND_ACK_REJECT:{
                        msg->data_sz = 1 + strlen(usr->name) + 1;
                        strcpy(data + 1, usr->name);
                        if (tgtusr->sockfd == -1) {
                            // offline
                            add_msg(msg);
                        } else {
                            // online
                            write(tgtusr->sockfd, (void*) msg, msgsize(msg));
                        }
                        break;}
                    case FRIEND_ACK_ALLOW:{
                        msg->data_sz = 1 + strlen(usr->name) + 1;
                        strcpy(data + 1, usr->name);
                        if (tgtusr->sockfd == -1) {
                            // offline
                            add_msg(msg);
                        } else {
                            // online
                            write(tgtusr->sockfd, (void*) msg, msgsize(msg));
                        }
                        add_friend(usr, tgtusr);
                        break;}
                    default:{
                        assert (0);
                        break;}
                }
                break;}

            case MSG_TYPE_FILE:{
                assert (msg->from_id == usr->id);
                assert (msg->to_id != -1);
                user_t* dstusr = get_user_byid(msg->to_id);
                if (dstusr->sockfd != -1) {
                    // dest online
                    write(dstusr->sockfd, (void*) msg, msgsize(msg));
                } else {
                    add_msg(msg);
                    // dest offline
                }
                int* file_status_code = (int*) (data + sizeof(int));
                if (*file_status_code == FILE_BEGIN || 
                        *file_status_code == FILE_TRANSFER) {
                    write(sockfd, (void*) buffer, 1);
                }
                break;}

            default:{
                printf("[%d] | unhandled msg, dump\n", tid);
                printf("[%d] | type: %d\n", tid, msg->type);
                printf("[%d] | from_id: %d\n", tid, msg->from_id);
                printf("[%d] | to_id: %d\n", tid, msg->to_id);
                printf("[%d] | data_sz: %d\n", tid, msg->data_sz);
                printf("[%d] | data dump ********\n", tid);
                dump_mem(msgdata(msg), msg->data_sz);
                printf("[%d] | ********\n", tid);
                break;}
        }
    }

    if (read_size == 0) printf("client disconnected\n");
    else if (read_size == -1) printf("recv failed\n");

    close(sockfd);
    if (usr != NULL) usr->sockfd = -1;

    free(_sockfd);
    return 0;
}


void* stdin_handler(void* dummy)
{
    char cmd[20];
    while (1) {
        fgets(cmd, sizeof(cmd), stdin);
        cmd[strlen(cmd) - 1] = 0; // strip '\n'
        if (!strcmp(cmd, "quit")) {
            finalize_usrmgr();
            finalize_msgbuf();
            exit(0);
        }
        printf("invalid command!\n");
    }
    return 0;
}
