#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>

#include "user.h"
#include "msgbuf.h"

void create_user_file()
{
    FILE* fout = fopen(USER_FILE, "w");

    fprintf(fout, "%d\n", 1);
    fprintf(fout, "%s\n", "usr1");
    fprintf(fout, "%s\n", "usr1pswd");
    fprintf(fout, "0 -1\n\n");

    fprintf(fout, "%d\n", 0);
    fprintf(fout, "%s\n", "admin");
    fprintf(fout, "%s\n\n", "adminpswd");
    fprintf(fout, "1 -1\n\n");

    fclose(fout);
}

void create_user_file_complex()
{
    FILE* fout = fopen(USER_FILE, "w");

    fprintf(fout, "%d\n", 3);
    fprintf(fout, "%s\n", "usr3");
    fprintf(fout, "%s\n", "usr3pswd");
    fprintf(fout, "0 -1\n\n");

    fprintf(fout, "%d\n", 2);
    fprintf(fout, "%s\n", "usr2");
    fprintf(fout, "%s\n", "usr2pswd");
    fprintf(fout, "0 -1\n\n");

    fprintf(fout, "%d\n", 1);
    fprintf(fout, "%s\n", "usr1");
    fprintf(fout, "%s\n", "usr1pswd");
    fprintf(fout, "0 -1\n\n");

    fprintf(fout, "%d\n", 0);
    fprintf(fout, "%s\n", "admin");
    fprintf(fout, "%s\n\n", "adminpswd");
    fprintf(fout, "1 -1\n\n");

    fclose(fout);
}


/*
 * covers:
 *  init_usrmgr
 *  add_user
 *  get_user
 *  finalize_usrmgr
 */
void test_user_basic()
{
    printf("%s\n", "test_user begin\n");
    create_user_file();

    init_usrmgr();
    _dump_usrmgr();
    printf("init_usrmgr complete\n");

#define TEST_NEW_USRS 5
#define TEST_NAME_LEN 5
#define TEST_PSWD_LEN 10
    char names[TEST_NEW_USRS][MAX_NAME_LEN];
    char pswds[TEST_NEW_USRS][MAX_PSWD_LEN];
    user_t* usrs[TEST_NEW_USRS];
    for (int k = 0; k < TEST_NEW_USRS; k++) {
        char name[TEST_NAME_LEN];
        name[TEST_NAME_LEN - 1] = 0;
        for (int i = 0; i < TEST_NAME_LEN - 1; i++)
            name[i] = 'a' + (rand() % 26);
        strcpy(names[k], name);
        char pswd[TEST_PSWD_LEN];
        pswd[TEST_PSWD_LEN - 1] = 0;
        for (int i = 0; i < TEST_PSWD_LEN - 1; i++)
            pswd[i] = 'a' + (rand() % 26);
        strcpy(pswds[k], pswd);
        printf("adding user: [%d] [%s] [%s]\n", k, name, pswd);

        assert ((usrs[k] = add_user(name, pswd)) != NULL);
    }
    assert (add_user("usr1", "whatever") == NULL);

    user_t* admin = get_user("admin", "adminpswd");
    assert (admin->id == 0);
    assert (!strcmp(admin->name, "admin"));
    int random_idx = rand() % TEST_NEW_USRS;
    user_t* random = get_user(names[random_idx], pswds[random_idx]);
    assert (random->id == 2 + random_idx);
    assert (!strcmp(random->pswd, pswds[random_idx]));
    user_t* admin_badpswd = get_user("admin", "adminbadpswd");
    assert (admin_badpswd == NULL);

    _dump_usrmgr();
    finalize_usrmgr();
    printf("\nib\n");
    init_usrmgr();
    _dump_usrmgr();

    admin = get_user_byname("admin");
    assert (admin != NULL);
    for (int i = 0; i < TEST_NEW_USRS; i++) {
        usrs[i] = get_user_byname(names[i]);
        assert (!strcmp(usrs[i]->pswd, pswds[i]));
    }
    for (int i = 0; i < TEST_NEW_USRS; i++) {
        add_friend(usrs[i], usrs[(i+1) % TEST_NEW_USRS]);
        add_friend(admin, usrs[i]);
    }

    finalize_usrmgr();
    init_usrmgr();
    _dump_usrmgr();
    finalize_usrmgr();
    printf("%s\n", "test_user success\n");
    getchar();
}

void test_msgbuf() 
{
    printf("%s\n", "test_msgbuf begin\n");
    create_user_file_complex();
    init_usrmgr();

    _dump_msg_list_all();
    msg_list_t* msglist = NULL;
    pull(2, &msglist);
    _dump_msg_list(msglist);
    _dump_msg_list_all();

    finalize_usrmgr();
    printf("%s\n", "test_msgbuf success\n");
    getchar();
}

int main()
{
    srand(time(0));
    test_user_basic();
    return 0;
}
