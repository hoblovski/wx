#include "msgbuf.h"
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>

msg_list_t* _msgbuf;

void init_msgbuf() 
{
    _msgbuf = NULL;
}

void finalize_msgbuf() 
{
    msg_list_t* msglist = _msgbuf;
    while (msglist) {
        msg_list_t* cur = msglist;
        msglist = msglist->next;
        free(cur);
    }
}

void add_msg(msg_t* msg) 
{
    msg_list_t* l = (msg_list_t*) malloc(sizeof(msg_list_t));
    l->msg = msg;
    l->next = _msgbuf;
    _msgbuf = l;
}

static inline void remove_msg(msg_list_t* prev, msg_list_t* msg) 
{
    if (prev != NULL) {
        prev->next = msg->next;
    } else {
        _msgbuf = msg->next;
    }
}

void free_msglist(msg_list_t* msglist, int recursive)
{
    msg_list_t* cur;
    while (msglist) {
        cur = msglist;
        msglist = msglist->next;
        if (recursive)
            free(cur->msg);
        free(cur);
    }
}

void pull(int to_id, msg_list_t** ret_msglist) 
{
    *ret_msglist = NULL;
    msg_list_t* prev = NULL;
    msg_list_t* msglist = _msgbuf;
    while (msglist) {
        msg_t* msg = msglist->msg;
        if (msg->to_id == to_id) {
            msg_list_t* item = (msg_list_t*) malloc(sizeof(msg_list_t));
            item->next = *ret_msglist;
            item->msg = msg;
            *ret_msglist = item;
            remove_msg(prev, msglist);
            msg_list_t* cur = msglist;
            msglist = msglist->next;
            free(cur);
        } else {
            prev = msglist;
            msglist = msglist->next;
        }
    }
}

char* _msg_type_repr(msg_type type)
{
    static char* a[] = {
        "MSG_TYPE_FILE",
        "MSG_TYPE_TEXT",
        "MSG_TYPE_FRIEND_REQ",
        "MSG_TYPE_FRIEND_ACK",
        "MSG_TYPE_FRIENDS_REQ",
        "MSG_TYPE_FRIENDS_ACK",
        "MSG_TYPE_LOGIN_REQ",
        "MSG_TYPE_LOGIN_RES"
    };
    return a[type];
}

void _dump_msg(msg_t* msg) 
{
    printf("%s\n", _msg_type_repr(msg->type));
    write(/*stdout==*/1, msgdata(msg), msg->data_sz);
    printf("\n");
    printf("from %d to %d\n\n", msg->from_id, msg->to_id);
}

void _dump_msg_list(msg_list_t* msglist)
{
    for (int i = 0; i < 40; i++) putchar('*');
    printf("dump_msg_list begin\n");
    while (msglist) {
        _dump_msg(msglist->msg);
        msglist = msglist->next;
    }
    for (int i = 0; i < 40; i++) putchar('*');
    printf("dump_msg_list finished\n");
}

void _dump_msg_list_all()
{
    _dump_msg_list(_msgbuf);
}


size_t msgsize(msg_t* msg)
{
    assert (msg->data_sz >= 0);
    return sizeof(msg->type) + sizeof(msg->from_id) + sizeof(msg->to_id)
            + sizeof(msg->data_sz) + msg->data_sz;
}

char* msgdata(msg_t* msg)
{
    return ((char*) &msg->data_sz) + sizeof(msg->data_sz);
}

size_t get_msgsize(size_t data_sz) {
    return sizeof(msg_type) + sizeof(int) + sizeof(int)
            + sizeof(int) + data_sz;
}
