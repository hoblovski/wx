#ifndef MTSERVER_H
#define MTSERVER_H

void mtserver_setup(int port, int* sockfd, int backlog);
void mtserver_mainloop(int sockfd,
        void* (*connection_handler)(void*),
        void* (*stdin_handler)(void*));

#endif // MTSERVER_H
