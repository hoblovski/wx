#include "common.h"

/*
 * iph: in host order
 */
void sockaddr_create(sockaddr_in* addr, int iph, short port) 
{
    bzero((char*) addr, sizeof(sockaddr_in));
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = htonl(iph);
    addr->sin_port = htons(port);
}

void sockaddr_create_ips(sockaddr_in* addr, char* ip, short port) 
{
    bzero((char*) addr, sizeof(sockaddr_in));
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = inet_addr(ip);
    addr->sin_port = htons(port);
}

void putb_hex(char v)
{
    static char num2hex[16] = "0123456789abcdef";
    putchar(num2hex[(v >> 4) & 0xf]);
    putchar(num2hex[v & 0xf]);
}

void dump_mem(char* v, int size) 
{
    for (int i = 0; i < size; i++) {
        putb_hex(v[i]);
        putchar(((i & 0xf) == 0xf) ? '\n' : ' ');
    }
    if ((size & 0xf) != 0xf)
        putchar('\n');
}
