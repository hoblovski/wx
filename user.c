#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "user.h"

struct usrmgr_t {
    user_t* head;
    int n_records;
};
typedef struct usrmgr_t usrmgr_t;
struct usrmgr_t* _mgr;


// clean the \n of fgets
static inline void clean_lf(char* s) 
{
    int l = strlen(s);
    s[l - 1] = 0;
}


void init_usrmgr()
{
    FILE* filein = fopen(USER_FILE, "r");

    int n_rec = 0;
    user_t* prev = NULL;
    user_t* head = NULL;

    while (1) {
        int id;
        if (fscanf(filein, "%d", &id) == EOF) break;
        while (fgetc(filein) != '\n') ;
        user_t* usr = create_user();
        usr->id = id;
        fgets(usr->name, MAX_NAME_LEN, filein);
        clean_lf(usr->name);
        fgets(usr->pswd, MAX_PSWD_LEN, filein);
        clean_lf(usr->pswd);
        usr->friends = NULL;
        int friend_usrid;
        while (1) {
            fscanf(filein, "%d", &friend_usrid);
            if (friend_usrid == -1) break;
            friend_list_t* friend =
                (friend_list_t*) malloc(sizeof(friend_list_t));
            friend->next = usr->friends;
            friend->usrid = friend_usrid;
            usr->friends = friend;
        }

        usr->next = NULL;
        if (prev != NULL)
            prev->next = usr;
        prev = usr;
        if (head == NULL)
            head = usr;
        n_rec++;
    }

    fclose(filein);

    _mgr = (usrmgr_t*) malloc(sizeof(usrmgr_t));
    _mgr->head = head;
    _mgr->n_records = n_rec;
}


user_t* add_user(char* name, char* pswd) 
{
    user_t* usr;
    // quit if already exists
    if ((usr = get_user_byname(name)) != NULL) {
        return NULL;
    }
    usr = create_user();
    usr->id = _mgr->n_records++;
    strcpy(usr->name, name);
    strcpy(usr->pswd, pswd);
    usr->next = _mgr->head;
    _mgr->head = usr;
    return usr;
}


void add_friend(user_t* one, user_t* other)
{
    if (one->id == other->id) return;
    friend_list_t* t = (friend_list_t*) malloc(sizeof(friend_list_t));
    t->next = one->friends;
    t->usrid = other->id;
    one->friends = t;
    t = (friend_list_t*) malloc(sizeof(friend_list_t));
    t->next = other->friends;
    t->usrid = one->id;
    other->friends = t;
}


user_t* get_user(char* name, char* pswd) 
{
    user_t* it = _mgr->head;
    while (it) {
        if (!strcmp(it->name, name) && !strcmp(it->pswd, pswd)) {
            return it;
        }
        it = it->next;
    }
    return NULL;
}

user_t* get_user_byname(char* name)
{
    user_t* it = _mgr->head;
    while (it) {
        if (!strcmp(it->name, name)) {
            return it;
        }
        it = it->next;
    }
    return NULL;
}


user_t* get_user_byid(int id) 
{
    user_t* it = _mgr->head;
    while (it) {
        if (it->id == id) return it;
        it = it->next;
    }
    return NULL;
}


void finalize_usrmgr()
{
    FILE* fout = fopen(USER_FILE, "w");
    user_t* it = _mgr->head;
    while (it) {
        fprintf(fout, "%d\n", it->id);
        fprintf(fout, "%s\n", it->name);
        fprintf(fout, "%s\n", it->pswd);
        friend_list_t* friend = it->friends;
        while (friend) {
            fprintf(fout, "%d ", friend->usrid);
            friend = friend->next;
        }
        fprintf(fout, "-1\n\n");
        it = it->next;
    }
    fclose(fout);
    it = _mgr->head;
    while (it) {
        user_t* cur = it;
        it = it->next;
        friend_list_t* f = cur->friends;
        while (f) {
            friend_list_t* fcur = f;
            f = f->next;
            free(fcur);
        }
        free(cur);
    }
    free(_mgr);
    _mgr = NULL;
}

void _dump_usrmgr()
{
    for (int i = 0; i < 40; i++) putchar('*');
    printf("dump_usrmgr begin\n");
    for (user_t* head = _mgr->head; head != NULL; head = head->next) {
        printf("id: %d\n", head->id);
        printf("name: %s\n", head->name);
        printf("pswd: %s\n", head->pswd);
        printf("friends:");
        friend_list_t* f = head->friends;
        while (f) {
            printf(" %d", f->usrid);
            f = f->next;
        }
        printf("\n\n");
    }
    for (int i = 0; i < 40; i++) putchar('*');
    printf("dump_usrmgr finished\n");
}


user_t* create_user()
{
    user_t* usr = (user_t*) malloc(sizeof(user_t));
    usr->id = -1;
    usr->name[0] = 0;
    usr->pswd[0] = 0;
    usr->friends = NULL;
    usr->sockfd = -1;
    usr->next = NULL;
    return usr;
}
