#ifndef MSGBUF_H
#define MSGBUF_H

#include "user.h"

#define DISALLOW 0x02
#define ALLOW 0x01

#define FRIEND_ACK_ALLOW 0x01
#define FRIEND_ACK_REJECT 0x02
#define FRIEND_ACK_NO_SUCH_USR 0x03

#define FILE_BEGIN 0x01
#define FILE_TRANSFER 0x02
#define FILE_END 0x03
#define FILE_SEND_SLACK 50

enum msg_type {
    MSG_TYPE_FILE = 0,
    MSG_TYPE_TEXT = 1,
    MSG_TYPE_FRIEND_REQ = 2,
    MSG_TYPE_FRIEND_ACK = 3,
    MSG_TYPE_FRIENDS_REQ = 4,
    MSG_TYPE_FRIENDS_ACK = 5,
    MSG_TYPE_LOGIN_REQ = 6,
    MSG_TYPE_LOGIN_RES = 7
};
typedef enum msg_type msg_type;

struct msg_t {
    msg_type type;
    int from_id;
    int to_id;

    int data_sz;
    // possibly data following
};

typedef struct msg_t msg_t;

struct msg_list_t {
    msg_t* msg;
    struct msg_list_t* next;
};
typedef struct msg_list_t msg_list_t;

void init_msgbuf();
size_t msgsize(msg_t* msg);
size_t get_msgsize(size_t data_sz);
char* msgdata(msg_t* msg);
void add_msg(msg_t* msg);
/* pull to_id will cause the returned entries to be EXTRACTED from queue */
void pull(int to_id, msg_list_t** msglist);
void free_msglist(msg_list_t* msglist, int recursive);
void finalize_msgbuf();

/* don't use these unless you know what you are doing */
void _dump_msg(msg_t* msg);
void _dump_msg_list(msg_list_t* msglist);
void _dump_msg_list_all();
char* _msg_type_repr(msg_type type);

#endif // MSGBUF_H
